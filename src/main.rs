#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

extern crate itertools;
//extern crate hyper;
extern crate curl;
//extern crate json_tools;
extern crate serde;
extern crate serde_json;
extern crate term;

#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
enum Planet {
    Barren,
    Gas,
    Ice,
    Oceanic,
    Storm,
    Temperate,
    Lava,
    Plasma,
}

impl Planet {
    fn all() -> Vec<Planet> {
        use Planet::*;
        vec![
            Barren,
            Gas,
            Ice,
            Oceanic,
            Storm,
            Temperate,
            Lava,
            Plasma,
        ]
    }

    fn raws(self) -> Vec<Raw> {
        Raw::all()
            .iter()
            .cloned()
            .filter(|raw| raw.planets().contains(&self))
            .collect()
    }

    fn t1s(self) -> Vec<T1> {
        self.raws().iter().map(|x| x.t1()).collect()
    }

    fn t2s(self) -> Vec<T2> {
        use itertools::Itertools;
        let combinations = self.t1s().iter().cloned().combinations().collect::<Vec<(T1, T1)>>();
        T2::all()
            .iter()
            .cloned()
            .filter(|t2| {
                let (t1_0, t1_1) = t2.t1s();
                combinations.contains(&(t1_0, t1_1)) || combinations.contains(&(t1_1, t1_0))
            })
            .collect()
    }

    fn t3s(self) -> Vec<T3> {
        use itertools::Itertools;
        let pairs = self.t2s().iter().cloned().combinations().collect::<Vec<(T2, T2)>>();
        let triples = self.t2s().iter().cloned().combinations_n(3).map(|x|(x[0],x[1],x[2])).collect::<Vec<(T2,T2,T2)>>();
        T3::all()
            .iter()
            .cloned()
            .filter(|t3| {
                let t2s = t3.t2s();
                match t2s {
                    (t2_0, t2_1, None) => pairs.contains(&(t2_0, t2_1)) || pairs.contains(&(t2_1, t2_0)),
                    (t2_0, t2_1, Some(t2_2)) => triples.contains(&(t2_0,t2_1,t2_2)) ||
                                                triples.contains(&(t2_0,t2_2,t2_1)) ||
                                                triples.contains(&(t2_1,t2_0,t2_1)) ||
                                                triples.contains(&(t2_1,t2_2,t2_0)) ||
                                                triples.contains(&(t2_2,t2_0,t2_1)) ||
                                                triples.contains(&(t2_2,t2_1,t2_0))
                }
            })
            .collect()
    }

    fn first_letter(self) -> String {
        use Planet::*;
        match self {
            Barren => "B",
            Gas => "G",
            Ice => "I",
            Oceanic => "O",
            Storm => "S",
            Temperate => "T",
            Lava => "L",
            Plasma => "P",
        }.to_owned()
    }
}

#[derive(Debug,Clone,Copy)]
enum Raw {
    MicroOrganisms,
    CarbonCompounds,
    PlankticColonies,
    NonCSCrystals,
    IonicSolutions,
    Autotrophs,
    ReactiveGas,
    NobleGas,
    SuspendedPlasma,
    NobleMetals,
    ComplexOrganisms,
    BaseMetals,
    FelsicMagma,
    HeavyMetals,
    AqueousLiquids,
}

impl Raw {
    fn all() -> Vec<Raw> {
        use Raw::*;
        vec![
            MicroOrganisms,
            CarbonCompounds,
            PlankticColonies,
            NonCSCrystals,
            IonicSolutions,
            Autotrophs,
            ReactiveGas,
            NobleGas,
            SuspendedPlasma,
            NobleMetals,
            ComplexOrganisms,
            BaseMetals,
            FelsicMagma,
            HeavyMetals,
            AqueousLiquids,
        ]
    }

    fn t1(self) -> T1 {
        use Raw::*;
        use T1::*;
        match self {
            MicroOrganisms => Bacteria,
            CarbonCompounds => Biofuels,
            PlankticColonies => Biomass,
            NonCSCrystals => ChiralStructures,
            IonicSolutions => Electrolytes,
            Autotrophs => IndustrialFibers,
            ReactiveGas => OxidizingCompound,
            NobleGas => Oxygen,
            SuspendedPlasma => Plasmoids,
            NobleMetals => PreciousMetals,
            ComplexOrganisms => Proteins,
            BaseMetals => ReactiveMetals,
            FelsicMagma => Silicon,
            HeavyMetals => ToxicMetals,
            AqueousLiquids => Water,
        }
    }

    fn planets(self) -> Vec<Planet> {
        use Raw::*;
        use Planet::*;
        match self {
            MicroOrganisms => vec![Barren, Ice, Oceanic, Temperate],
            CarbonCompounds => vec![Barren, Oceanic, Temperate],
            PlankticColonies => vec![Ice, Oceanic],
            NonCSCrystals => vec![Lava, Plasma],
            IonicSolutions => vec![Gas, Storm],
            Autotrophs => vec![Temperate],
            ReactiveGas => vec![Gas],
            NobleGas => vec![Gas, Ice, Storm],
            SuspendedPlasma => vec![Lava, Plasma, Storm],
            NobleMetals => vec![Barren, Plasma],
            ComplexOrganisms => vec![Oceanic, Temperate],
            BaseMetals => vec![Barren, Gas, Lava, Plasma, Storm],
            FelsicMagma => vec![Lava],
            HeavyMetals => vec![Ice, Lava, Plasma],
            AqueousLiquids => vec![Barren, Gas, Ice, Oceanic, Storm, Temperate],
        }
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord)]
enum T1 {
    Bacteria,
    Biofuels,
    Biomass,
    ChiralStructures,
    Electrolytes,
    IndustrialFibers,
    OxidizingCompound,
    Oxygen,
    Plasmoids,
    PreciousMetals,
    Proteins,
    ReactiveMetals,
    Silicon,
    ToxicMetals,
    Water,
}

impl T1 {
    fn all() -> Vec<T1> {
        use T1::*;
        vec![
            Bacteria,
            Biofuels,
            Biomass,
            ChiralStructures,
            Electrolytes,
            IndustrialFibers,
            OxidizingCompound,
            Oxygen,
            Plasmoids,
            PreciousMetals,
            Proteins,
            ReactiveMetals,
            Silicon,
            ToxicMetals,
            Water,
        ]
    }

    fn ids () -> Vec<u64> {
        T1::all().iter().map(|x|x.id()).collect()
    }

    fn raw(self) -> Raw {
        for raw in Raw::all() {
            if raw.t1() == self {
                return raw;
            }
        }
        unreachable!();
    }

    fn planets(self) -> Vec<Planet> {
        self.raw().planets()
    }

    fn abr() -> String {
        //TODO
        String::new()
    }

    fn id(self) -> u64 {
        use T1::*;
        match self {
            Bacteria => 2393,
            Biofuels => 2396,
            Biomass => 3779,
            ChiralStructures => 2401,
            Electrolytes => 2390,
            IndustrialFibers => 2397,
            OxidizingCompound => 2392,
            Oxygen => 3683,
            Plasmoids => 2389,
            PreciousMetals => 2399,
            Proteins => 2395,
            ReactiveMetals => 2398,
            Silicon => 9828,
            ToxicMetals => 2400,
            Water => 3645,
        }
    }

    fn from_id (id: u64) -> Option<T1> {
        for t1 in T1::all() {
            if t1.id() == id {
                return Some(t1);
            }
        }
        None
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }

    fn is_harvestable_in (self, solar_system: &[Planet]) -> bool {
        solar_system.iter().cloned().filter(|p| self.planets().contains(p)).collect::<Vec<Planet>>().len() > 0
    }

    fn harvestable_at (self, solar_system: &[Planet]) -> Vec<usize> {
        solar_system.iter().cloned().enumerate().filter(|&(i,p)| self.planets().contains(&p)).map(|(i,p)|i+1).collect::<Vec<usize>>()
    }
}

use std::fmt;

impl fmt::Display for T1 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T1::*;
        write!(f, "{: >18}", match *self {
           Bacteria          =>  "Bacteria",
           Biofuels          =>  "Biofuels",
           Biomass           =>  "Biomass",
           ChiralStructures  =>  "Chiral Structures",
           Electrolytes      =>  "Electrolytes",
           IndustrialFibers  =>  "Industrial Fibers",
           OxidizingCompound =>  "Oxidizing Compound",
           Oxygen            =>  "Oxygen",
           Plasmoids         =>  "Plasmoids",
           PreciousMetals    =>  "Precious Metals",
           Proteins          =>  "Proteins",
           ReactiveMetals    =>  "Reactive Metals",
           Silicon           =>  "Silicon",
           ToxicMetals       =>  "Toxic Metals",
           Water             =>  "Water",      
        })
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord)]
enum T2 {
    Biocells,
    ConstructionBlocks,
    ConsumerElectronics,
    Coolant,
    EnrichedUranium,
    Fertilizer,
    GenEnhancedLivestock,
    Livestock,
    MechanicalParts,
    MicrofiberShielding,
    MiniatureElectronics,
    Nanites,
    Oxides,
    Polyaramids,
    Polytextiles,
    RocketFuel,
    SilicateGlass,
    Superconductors,
    SupertensilePlastics,
    SyntheticOil,
    TestCultures,
    Transmitter,
    ViralAgent,
    WaterCooledCPU,
}

impl T2 {
    fn all() -> Vec<T2> {
        use T2::*;
        vec![
            Biocells             ,
            ConstructionBlocks   ,
            ConsumerElectronics  ,
            Coolant              ,
            EnrichedUranium      ,
            Fertilizer           ,
            GenEnhancedLivestock ,
            Livestock            ,
            MechanicalParts      ,
            MicrofiberShielding  ,
            MiniatureElectronics ,
            Nanites              ,
            Oxides               ,
            Polyaramids          ,
            Polytextiles         ,
            RocketFuel           ,
            SilicateGlass        ,
            Superconductors      ,
            SupertensilePlastics ,
            SyntheticOil         ,
            TestCultures         ,
            Transmitter          ,
            ViralAgent           ,
            WaterCooledCPU       ,
        ]
    }

    fn t1s(self) -> (T1, T1) {
        use T1::*;
        use T2::*;
        match self {
            Biocells => (PreciousMetals, Biofuels),
            ConstructionBlocks => (ToxicMetals, ReactiveMetals),
            ConsumerElectronics => (ChiralStructures, ToxicMetals),
            Coolant => (Water, Electrolytes),
            EnrichedUranium => (ToxicMetals, PreciousMetals),
            Fertilizer => (Proteins, Bacteria),
            GenEnhancedLivestock => (Biomass, Proteins),
            Livestock => (Biofuels, Proteins),
            MechanicalParts => (PreciousMetals, ReactiveMetals),
            MicrofiberShielding => (Silicon, IndustrialFibers),
            MiniatureElectronics => (Silicon, ChiralStructures),
            Nanites => (ReactiveMetals, Bacteria),
            Oxides => (Oxygen, OxidizingCompound),
            Polyaramids => (IndustrialFibers, OxidizingCompound),
            Polytextiles => (IndustrialFibers, Biofuels),
            RocketFuel => (Electrolytes, Plasmoids),
            SilicateGlass => (Silicon, OxidizingCompound),
            Superconductors => (Water, Plasmoids),
            SupertensilePlastics => (Biomass, Oxygen),
            SyntheticOil => (Oxygen, Electrolytes),
            TestCultures => (Water, Bacteria),
            Transmitter => (ChiralStructures, Plasmoids),
            ViralAgent => (Biomass, Bacteria),
            WaterCooledCPU => (Water, ReactiveMetals),
        }
    }

    fn planets(self) -> Vec<Planet> {
        let (t10, t11) = self.t1s();
        Planet::all().iter().cloned().filter(|p| t10.planets().contains(p) && t11.planets().contains(p)).collect()
    }

    fn id(self) -> u64 {
        use T2::*;
        match self {
            Biocells             => 2329,
            ConstructionBlocks   => 3828,
            ConsumerElectronics  => 9836,
            Coolant              => 9832,
            EnrichedUranium      => 44,
            Fertilizer           => 3693,
            GenEnhancedLivestock => 15317,
            Livestock            => 3725,
            MechanicalParts      => 3689,
            MicrofiberShielding  => 2327,
            MiniatureElectronics => 9842,
            Nanites              => 2463,
            Oxides               => 2317,
            Polyaramids          => 2321,
            Polytextiles         => 3695,
            RocketFuel           => 9830,
            SilicateGlass        => 3697,
            Superconductors      => 9838,
            SupertensilePlastics => 2312,
            SyntheticOil         => 3691,
            TestCultures         => 2319,
            Transmitter          => 9840,
            ViralAgent           => 3775,
            WaterCooledCPU       => 2328,
        }
    }

    fn from_id (id: u64) -> Option<T2> {
        for t2 in T2::all() {
            if t2.id() == id {
                return Some(t2);
            }
        }
        None
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }

    fn is_harvestable_in (self, solar_system: &[Planet]) -> bool {
        let (t10,t11) = self.t1s();
        t10.is_harvestable_in(solar_system) && t11.is_harvestable_in(solar_system)
    }
}

impl fmt::Display for T2 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T2::*;
        write!(f, "{: >30}", match *self {
            Biocells             => "Biocells"             ,
            ConstructionBlocks   => "Construction Blocks"   ,
            ConsumerElectronics  => "Consumer Electronics"  ,
            Coolant              => "Coolant"              ,
            EnrichedUranium      => "Enriched Uranium"      ,
            Fertilizer           => "Fertilizer"           ,
            GenEnhancedLivestock => "Genetically Enhanced Livestock" ,
            Livestock            => "Livestock"            ,
            MechanicalParts      => "Mechanical Parts"      ,
            MicrofiberShielding  => "Microfiber Shielding"  ,
            MiniatureElectronics => "Miniature Electronics" ,
            Nanites              => "Nanites"              ,
            Oxides               => "Oxides"               ,
            Polyaramids          => "Polyaramids"          ,
            Polytextiles         => "Polytextiles"         ,
            RocketFuel           => "Rocket Fuel"           ,
            SilicateGlass        => "Silicate Glass"        ,
            Superconductors      => "Superconductors"      ,
            SupertensilePlastics => "Supertensile Plastics" ,
            SyntheticOil         => "Synthetic Oil"         ,
            TestCultures         => "Test Cultures"         ,
            Transmitter          => "Transmitter"          ,
            ViralAgent           => "Viral Agent"           ,
            WaterCooledCPU       => "Water-Cooled CPU"       ,
        })
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord)]
enum T3 {
    BiotechResearchReports       ,
    CameraDrones                 ,
    Condensates                  ,
    CryoprotectantSolution       ,
    DataChips                    ,
    GelMatrixBiopaste            ,
    GuidanceSystems              ,
    HazmatDetectionSystems       ,
    HermeticMembranes            ,
    HighTechTransmitters         ,
    IndustrialExplosives         ,
    Neocoms                      ,
    NuclearReactors              ,
    PlanetaryVehicles            ,
    Robotics                     ,
    SmartfabUnits                ,
    Supercomputers               ,
    SyntheticSynapses            ,
    TranscranialMicrocontrollers ,
    UkomiSuperconductors         ,
    Vaccines                     ,
}

impl T3 {
    fn all () -> Vec<T3> {
        use T3::*;
        vec![
            BiotechResearchReports       ,
            CameraDrones                 ,
            Condensates                  ,
            CryoprotectantSolution       ,
            DataChips                    ,
            GelMatrixBiopaste            ,
            GuidanceSystems              ,
            HazmatDetectionSystems       ,
            HermeticMembranes            ,
            HighTechTransmitters         ,
            IndustrialExplosives         ,
            Neocoms                      ,
            NuclearReactors              ,
            PlanetaryVehicles            ,
            Robotics                     ,
            SmartfabUnits                ,
            Supercomputers               ,
            SyntheticSynapses            ,
            TranscranialMicrocontrollers ,
            UkomiSuperconductors         ,
            Vaccines                     ,
        ]
    }

    fn t2s (self) -> (T2,T2,Option<T2>) {
        use T3::*;
        use T2::*;
        match self {
            BiotechResearchReports       => (Nanites                , Livestock              , Some(ConstructionBlocks)    ),
            CameraDrones                 => (SilicateGlass          , RocketFuel             , None                     ),
            Condensates                  => (Oxides                 , Coolant                , None                     ),
            CryoprotectantSolution       => (TestCultures           , SyntheticOil           , Some(Fertilizer)            ),
            DataChips                    => (SupertensilePlastics   , MicrofiberShielding    , None                     ),
            GelMatrixBiopaste            => (Oxides                 , Biocells               , Some(Superconductors)       ),
            GuidanceSystems              => (WaterCooledCPU         , Transmitter            , None                     ),
            HazmatDetectionSystems       => (Polytextiles           , ViralAgent             , Some(Transmitter)           ),
            HermeticMembranes            => (Polyaramids            , GenEnhancedLivestock   , None                     ),
            HighTechTransmitters         => (Polyaramids            , Transmitter            , None                     ),
            IndustrialExplosives         => (Fertilizer             , Polytextiles           , None                     ),
            Neocoms                      => (Biocells               , SilicateGlass          , None                     ),
            NuclearReactors              => (MicrofiberShielding    , EnrichedUranium        , None                     ),
            PlanetaryVehicles            => (SupertensilePlastics   , MechanicalParts        , Some(MiniatureElectronics)  ),
            Robotics                     => (MechanicalParts        , ConsumerElectronics    , None                     ),
            SmartfabUnits                => (ConstructionBlocks     , MiniatureElectronics   , None                     ),
            Supercomputers               => (WaterCooledCPU         , Coolant                , Some(ConsumerElectronics)   ),
            SyntheticSynapses            => (SupertensilePlastics   , TestCultures           , None                     ),
            TranscranialMicrocontrollers => (Biocells               , Nanites                , None                     ),
            UkomiSuperconductors         => (SyntheticOil           , Superconductors        , None                     ),
            Vaccines                     => (Livestock              , ViralAgent             , None                     ),
        }
    }

    fn t1s (self) -> Vec<T1> {
        let t2s = self.t2s();
        let mut t1s = vec![
            t2s.0.t1s().0,
            t2s.0.t1s().1,
            t2s.1.t1s().0,
            t2s.1.t1s().1,
        ];
        if let Some(t2) = t2s.2 {
            t1s.push(t2.t1s().0);
            t1s.push(t2.t1s().1);
        }
        t1s
    }

    fn planets(self) -> Vec<Planet> {
        Planet::all().iter().cloned().filter(|p| {
            match self.t2s() {
                (t20,t21,Some(t22)) => {
                    let (t1a, t1b) = t20.t1s();
                    let (t1c, t1d) = t21.t1s();
                    let (t1e, t1f) = t22.t1s();
                    t1a.planets().contains(p) &&
                    t1b.planets().contains(p) &&
                    t1c.planets().contains(p) &&
                    t1d.planets().contains(p) &&
                    t1e.planets().contains(p) &&
                    t1f.planets().contains(p)
                } 
                (t20,t21,None) => {
                    let (t1a, t1b) = t20.t1s();
                    let (t1c, t1d) = t21.t1s();
                    t1a.planets().contains(p) &&
                    t1b.planets().contains(p) &&
                    t1c.planets().contains(p) &&
                    t1d.planets().contains(p)
                } 
            }
        }).collect()
    }

    fn id(self) -> u64 {
        use T3::*;
        match self {
            BiotechResearchReports       => 2358,
            CameraDrones                 => 2345,
            Condensates                  => 2344,
            CryoprotectantSolution       => 2367,
            DataChips                    => 17392,
            GelMatrixBiopaste            => 2348,
            GuidanceSystems              => 9834,
            HazmatDetectionSystems       => 2366,
            HermeticMembranes            => 2361,
            HighTechTransmitters         => 17898,
            IndustrialExplosives         => 2360,
            Neocoms                      => 2354,
            NuclearReactors              => 2352,
            PlanetaryVehicles            => 9846,
            Robotics                     => 9848,
            SmartfabUnits                => 2351,
            Supercomputers               => 2349,
            SyntheticSynapses            => 2346,
            TranscranialMicrocontrollers => 12836,
            UkomiSuperconductors         => 17136,
            Vaccines                     => 28974,
        }
    }

    fn from_id (id: u64) -> Option<T3> {
        for t3 in T3::all() {
            if t3.id() == id {
                return Some(t3);
            }
        }
        None
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }

    fn is_harvestable_in (self, solar_system: &[Planet]) -> bool {
        match self.t2s() {
            (t20,t21,Some(t22)) => t20.is_harvestable_in(solar_system) && t21.is_harvestable_in(solar_system) && t22.is_harvestable_in(solar_system),
            (t20,t21,None) => t20.is_harvestable_in(solar_system) && t21.is_harvestable_in(solar_system),
        }
    }
}

impl fmt::Display for T3 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T3::*;
        write!(f, "{: >30}", match *self {
            BiotechResearchReports        => "Biotech Research Reports" ,
            CameraDrones                  => "Camera Drones"  ,
            Condensates                   => "Condensates"  ,
            CryoprotectantSolution        => "Cryoprotectant Solution" ,
            DataChips                     => "Data Chips"  ,
            GelMatrixBiopaste             => "Gel-Matrix Biopaste" ,
            GuidanceSystems               => "Guidance Systems" ,
            HazmatDetectionSystems        => "Hazmat Detection Systems" ,
            HermeticMembranes             => "Hermetic Membranes"  ,
            HighTechTransmitters          => "High-Tech Transmitters"  ,
            IndustrialExplosives          => "Industrial Explosives"  ,
            Neocoms                       => "Neocoms" ,
            NuclearReactors               => "Nuclear Reactors" ,
            PlanetaryVehicles             => "Planetary Vehicles" ,
            Robotics                      => "Robotics" ,
            SmartfabUnits                 => "Smartfab Units"  ,
            Supercomputers                => "Supercomputers"  ,
            SyntheticSynapses             => "Synthetic Synapses" ,
            TranscranialMicrocontrollers  => "Transcranial Microcontrollers"  ,
            UkomiSuperconductors          => "Ukomi Superconductors"  ,
            Vaccines                      => "Vaccines"  ,
        })                                   
    }                                        
}                                            

#[derive(Serialize, Deserialize, Debug)]
struct ForQuery {
    bid: Option<bool>,
    types: Vec<u64>,
    regions: Vec<u64>,
    systems: Vec<u64>,
    hours: u64,
    minq: u64,
}

#[derive(Serialize, Deserialize, Debug)]
struct Unknown2 {
    forQuery: ForQuery,
    volume: u64,
    wavg: f64,
    avg: f64,
    variance: f64,
    stdDev: f64,
    median: f64,
    fivePercent: f64,
    max: f64,
    min: f64,
    highToLow: bool,
    generated: u64,
}

#[derive(Serialize, Deserialize, Debug)]
struct Unknown {
    buy: Unknown2,
    all: Unknown2,
    sell:Unknown2, 
}

trait Abbr {
    fn abbr (&self) -> String;
}

impl Abbr for Vec<Planet> {
    fn abbr (&self) -> String {
        Planet::all().iter().map(|p|if self.contains(p) { p.first_letter() } else { "_".to_owned() }).collect()
    }
}

fn main() {
    use Planet::*;
    //use hyper::Client;
    use std::error::Error;
    use curl::http;
    //use json_tools::Lexer;
    use itertools::Itertools;
    use std::collections::BTreeMap;

    let j141332 = vec![
        Lava,
        Barren,
        Barren,
        Oceanic,
        Barren,
        Gas,
        Gas,
        Gas,
        Barren,
        Barren,
    ];

    let j145659 = vec![
        Barren,
        Barren,
        Oceanic,
        Lava,
        Temperate,
        Temperate,
        Temperate,
        Oceanic,
    ];

    //let current = &j141332;
    let current = &j145659;

    println!("");
    println!("{}", current.abbr());
    println!("");

    // for planet in current {
    //    println!("{:?}\t{:?}\t{:?}", planet, planet.t1s(), planet.t2s());
    // }

    //for planet in Planet::all() {
    //    println!("{:?}\t{:?}\t{:?}\t{:?}", planet, planet.t1s(), planet.t2s(), planet.t3s());
    //}

    //for t1 in T1::all() {
    //    println!("{}", t1.url());
    //}

    //TODO use svg to draw infographics (diagrams, graphs) for every planet

    /*
    //XXX use curl until #531 will be merged in (https://github.com/hyperium/hyper/issues/531)
    let client = Client::new();
    let res = match client.get(&T1::Bacteria.url()).send() {
        Ok(res) => res,
        Err(e) => {
            println!("{} {}", e.description(), match e.cause() {
                Some(s) => format!("({})", s),
                None => "".to_owned(),
            });
            return;
        }
    };
    assert_eq!(res.status, hyper::Ok);
    */

    let mut handle = http::handle();

    println!("");

    let mut t1_prices = BTreeMap::new();
    let mut t2_prices = BTreeMap::new();
    let mut t3_prices = BTreeMap::new();
    let mut request = "https://api.eve-central.com/api/marketstat/json?usesystem=30000142".to_owned();
    for t1 in T1::all() {
        request.push_str(&format!("&typeid={}", t1.id()));
    }
    for t2 in T2::all() {
        request.push_str(&format!("&typeid={}", t2.id()));
    }
    for t3 in T3::all() {
        request.push_str(&format!("&typeid={}", t3.id()));
    }
    let resp = handle.get(request.as_str()).exec().unwrap();
    if resp.get_code() == 200 {
        let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
        let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
        for item in des {
            //println!("{:#?}", item);
            let id = item.buy.forQuery.types[0];
            let avg = item.buy.avg;
            let wavg = item.buy.wavg;
            if let Some(t1) = T1::from_id(id) {
                if let Some(previous) = t1_prices.insert(t1, (avg,wavg)) { panic!("same item twice!!!"); }
            }
            if let Some(t2) = T2::from_id(id) {
                if let Some(previous) = t2_prices.insert(t2, (avg,wavg)) { panic!("same item twice!!!"); }
            }
            if let Some(t3) = T3::from_id(id) {
                if let Some(previous) = t3_prices.insert(t3, (avg,wavg)) { panic!("same item twice!!!"); }
            }
        }
    }

    let mut t = term::stdout().unwrap();

    let mut t1_avg_wavg = T1::all().iter().map(|&t1|{
        let &(avg,wavg) = t1_prices.get(&t1).unwrap_or(&(0.0,0.0));
        (t1,avg,wavg)
    }).collect::<Vec<(T1,f64,f64)>>();
    t1_avg_wavg.sort_by_key(|a|a.2.trunc() as u64);

    for (t1,avg,wavg) in t1_avg_wavg {

        if t1.is_harvestable_in(current) {
            t.fg(term::color::GREEN).unwrap();
        } else {
            t.fg(term::color::RED).unwrap();
        }
        writeln!(t, "{} {:10.2} {:10.2} {}", t1, avg, wavg, t1.planets().abbr()).unwrap();
        t.reset().unwrap();
    }

    println!("");

    let mut t2_avg_wavg = T2::all().iter().map(|&t2|{
        let &(avg,wavg) = t2_prices.get(&t2).unwrap_or(&(0.0,0.0));
        (t2,avg,wavg)
    }).collect::<Vec<(T2,f64,f64)>>();
    t2_avg_wavg.sort_by_key(|a|a.2.trunc() as u64);

    for (t2,avg,wavg) in t2_avg_wavg {

        if t2.is_harvestable_in(current) {
            t.fg(term::color::GREEN).unwrap();
        } else {
            t.fg(term::color::RED).unwrap();
        }
        writeln!(t, "{} {:10.2} {:10.2} {}", t2, avg, wavg, t2.planets().abbr()).unwrap();
        t.reset().unwrap();
    }

    println!("");

    let mut t3_avg_wavg = T3::all().iter().map(|&t3|{
        let &(avg,wavg) = t3_prices.get(&t3).unwrap_or(&(0.0,0.0));
        (t3,avg,wavg)
    }).collect::<Vec<(T3,f64,f64)>>();
    t3_avg_wavg.sort_by_key(|a|a.2.trunc() as u64);

    for (t3,avg,wavg) in t3_avg_wavg {

        if t3.is_harvestable_in(current) {
            t.fg(term::color::GREEN).unwrap();
        } else {
            t.fg(term::color::RED).unwrap();
        }
        writeln!(t, "{} {:10.2} {:10.2} {}", t3, avg, wavg, t3.planets().abbr()).unwrap();
        t.reset().unwrap();
    }

    println!("");
    println!("T3::HazmatDetectionSystems");
    for t1 in T3::HazmatDetectionSystems.t1s() {
        println!("{} {:?}", t1, t1.harvestable_at(current));
    }

    println!("");
    println!("T3::BiotechResearchReports");
    for t1 in T3::BiotechResearchReports.t1s() {
        println!("{} {:?}", t1, t1.harvestable_at(current));
    }

    println!("");
    println!("T3::NuclearReactors");
    for t1 in T3::NuclearReactors.t1s() {
        println!("{} {:?}", t1, t1.harvestable_at(current));
    }

    /*
    println!("");

    for planet in current.iter().unique() {
        println!("{:?}", planet);
        for t1 in planet.t1s() {
            let resp = handle.get(t1.url().as_str()).exec().unwrap();
            if resp.get_code() == 200 {
                let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
                let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
                println!("{} {:10.2} {:10.2} {}", t1, des[0].buy.avg, des[0].buy.wavg, t1.planets().abbr());
            }
        }
        for t2 in planet.t2s() {
            let resp = handle.get(t2.url().as_str()).exec().unwrap();
            if resp.get_code() == 200 {
                let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
                let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
                println!("{} {:10.2} {:10.2} {}", t2, des[0].buy.avg, des[0].buy.wavg, t2.planets().abbr());
            }
        }
        println!("");
    }
    */
}
