#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

extern crate itertools;
//extern crate hyper;
extern crate curl;
//extern crate json_tools;
extern crate serde;
extern crate serde_json;

#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
enum Planet {
    Barren,
    Gas,
    Ice,
    Oceanic,
    Storm,
    Temperate,
    Lava,
    Plasma,
}

impl Planet {
    fn all() -> Vec<Planet> {
        use Planet::*;
        vec![
            Barren,
            Gas,
            Ice,
            Oceanic,
            Storm,
            Temperate,
            Lava,
            Plasma,
        ]
    }

    fn raws(self) -> Vec<Raw> {
        Raw::all()
            .iter()
            .cloned()
            .filter(|raw| raw.planets().contains(&self))
            .collect()
    }

    fn t1s(self) -> Vec<T1> {
        self.raws().iter().map(|x| x.t1()).collect()
    }

    fn t2s(self) -> Vec<T2> {
        use itertools::Itertools;
        let combinations = self.t1s().iter().cloned().combinations().collect::<Vec<(T1, T1)>>();
        T2::all()
            .iter()
            .cloned()
            .filter(|t2| {
                let (t1_0, t1_1) = t2.t1s();
                combinations.contains(&(t1_0, t1_1)) || combinations.contains(&(t1_1, t1_0))
            })
            .collect()
    }

    fn t3s(self) -> Vec<T3> {
        use itertools::Itertools;
        let pairs = self.t2s().iter().cloned().combinations().collect::<Vec<(T2, T2)>>();
        let triples = self.t2s().iter().cloned().combinations_n(3).map(|x|(x[0],x[1],x[2])).collect::<Vec<(T2,T2,T2)>>();
        T3::all()
            .iter()
            .cloned()
            .filter(|t3| {
                let t2s = t3.t2s();
                match t2s {
                    (t2_0, t2_1, None) => pairs.contains(&(t2_0, t2_1)) || pairs.contains(&(t2_1, t2_0)),
                    (t2_0, t2_1, Some(t2_2)) => triples.contains(&(t2_0,t2_1,t2_2)) ||
                                                triples.contains(&(t2_0,t2_2,t2_1)) ||
                                                triples.contains(&(t2_1,t2_0,t2_1)) ||
                                                triples.contains(&(t2_1,t2_2,t2_0)) ||
                                                triples.contains(&(t2_2,t2_0,t2_1)) ||
                                                triples.contains(&(t2_2,t2_1,t2_0))
                }
            })
            .collect()
    }

    fn first_letter(self) -> String {
        use Planet::*;
        match self {
            Barren => "B",
            Gas => "G",
            Ice => "I",
            Oceanic => "O",
            Storm => "S",
            Temperate => "T",
            Lava => "L",
            Plasma => "P",
        }.to_owned()
    }
}

#[derive(Debug,Clone,Copy)]
enum Raw {
    MicroOrganisms,
    CarbonCompounds,
    PlankticColonies,
    NonCSCrystals,
    IonicSolutions,
    Autotrophs,
    ReactiveGas,
    NobleGas,
    SuspendedPlasma,
    NobleMetals,
    ComplexOrganisms,
    BaseMetals,
    FelsicMagma,
    HeavyMetals,
    AqueousLiquids,
}

impl Raw {
    fn all() -> Vec<Raw> {
        use Raw::*;
        vec![
            MicroOrganisms,
            CarbonCompounds,
            PlankticColonies,
            NonCSCrystals,
            IonicSolutions,
            Autotrophs,
            ReactiveGas,
            NobleGas,
            SuspendedPlasma,
            NobleMetals,
            ComplexOrganisms,
            BaseMetals,
            FelsicMagma,
            HeavyMetals,
            AqueousLiquids,
        ]
    }

    fn t1(self) -> T1 {
        use Raw::*;
        use T1::*;
        match self {
            MicroOrganisms => Bacteria,
            CarbonCompounds => Biofuels,
            PlankticColonies => Biomass,
            NonCSCrystals => ChiralStructures,
            IonicSolutions => Electrolytes,
            Autotrophs => IndustrialFibers,
            ReactiveGas => OxidizingCompound,
            NobleGas => Oxygen,
            SuspendedPlasma => Plasmoids,
            NobleMetals => PreciousMetals,
            ComplexOrganisms => Proteins,
            BaseMetals => ReactiveMetals,
            FelsicMagma => Silicon,
            HeavyMetals => ToxicMetals,
            AqueousLiquids => Water,
        }
    }

    fn planets(self) -> Vec<Planet> {
        use Raw::*;
        use Planet::*;
        match self {
            MicroOrganisms => vec![Barren, Ice, Oceanic, Temperate],
            CarbonCompounds => vec![Barren, Oceanic, Temperate],
            PlankticColonies => vec![Ice, Oceanic],
            NonCSCrystals => vec![Lava, Plasma],
            IonicSolutions => vec![Gas, Storm],
            Autotrophs => vec![Temperate],
            ReactiveGas => vec![Gas],
            NobleGas => vec![Gas, Ice, Storm],
            SuspendedPlasma => vec![Lava, Plasma, Storm],
            NobleMetals => vec![Barren, Plasma],
            ComplexOrganisms => vec![Oceanic, Temperate],
            BaseMetals => vec![Barren, Gas, Lava, Plasma, Storm],
            FelsicMagma => vec![Lava],
            HeavyMetals => vec![Ice, Lava, Plasma],
            AqueousLiquids => vec![Barren, Gas, Ice, Oceanic, Storm, Temperate],
        }
    }
}

#[derive(Debug,Clone,Copy,PartialEq)]
enum T1 {
    Bacteria,
    Biofuels,
    Biomass,
    ChiralStructures,
    Electrolytes,
    IndustrialFibers,
    OxidizingCompound,
    Oxygen,
    Plasmoids,
    PreciousMetals,
    Proteins,
    ReactiveMetals,
    Silicon,
    ToxicMetals,
    Water,
}

impl T1 {
    fn all() -> Vec<T1> {
        use T1::*;
        vec![
            Bacteria,
            Biofuels,
            Biomass,
            ChiralStructures,
            Electrolytes,
            IndustrialFibers,
            OxidizingCompound,
            Oxygen,
            Plasmoids,
            PreciousMetals,
            Proteins,
            ReactiveMetals,
            Silicon,
            ToxicMetals,
            Water,
        ]
    }

    fn raw(self) -> Raw {
        for raw in Raw::all() {
            if raw.t1() == self {
                return raw;
            }
        }
        unreachable!();
    }

    fn planets(self) -> Vec<Planet> {
        self.raw().planets()
    }

    fn abr() -> String {
        //TODO
        String::new()
    }

    fn id(self) -> u64 {
        use T1::*;
        match self {
            Bacteria => 2393,
            Biofuels => 2396,
            Biomass => 3779,
            ChiralStructures => 2401,
            Electrolytes => 2390,
            IndustrialFibers => 2397,
            OxidizingCompound => 2392,
            Oxygen => 3683,
            Plasmoids => 2389,
            PreciousMetals => 2399,
            Proteins => 2395,
            ReactiveMetals => 2398,
            Silicon => 9828,
            ToxicMetals => 2400,
            Water => 3645,
        }
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }
}

use std::fmt;

impl fmt::Display for T1 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T1::*;
        write!(f, "{: >18}", match *self {
           Bacteria          =>  "Bacteria",
           Biofuels          =>  "Biofuels",
           Biomass           =>  "Biomass",
           ChiralStructures  =>  "Chiral Structures",
           Electrolytes      =>  "Electrolytes",
           IndustrialFibers  =>  "Industrial Fibers",
           OxidizingCompound =>  "Oxidizing Compound",
           Oxygen            =>  "Oxygen",
           Plasmoids         =>  "Plasmoids",
           PreciousMetals    =>  "Precious Metals",
           Proteins          =>  "Proteins",
           ReactiveMetals    =>  "Reactive Metals",
           Silicon           =>  "Silicon",
           ToxicMetals       =>  "Toxic Metals",
           Water             =>  "Water",      
        })
    }
}

#[derive(Debug,Clone,Copy,PartialEq)]
enum T2 {
    Biocells,
    ConstructionBlocks,
    ConsumerElectronics,
    Coolant,
    EnrichedUranium,
    Fertilizer,
    GenEnhancedLivestock,
    Livestock,
    MechanicalParts,
    MicrofiberShielding,
    MiniatureElectronics,
    Nanites,
    Oxides,
    Polyaramids,
    Polytextiles,
    RocketFuel,
    SilicateGlass,
    Superconductors,
    SupertensilePlastics,
    SyntheticOil,
    TestCultures,
    Transmitter,
    ViralAgent,
    WaterCooledCPU,
}

impl T2 {
    fn all() -> Vec<T2> {
        use T2::*;
        vec![
            Biocells             ,
            ConstructionBlocks   ,
            ConsumerElectronics  ,
            Coolant              ,
            EnrichedUranium      ,
            Fertilizer           ,
            GenEnhancedLivestock ,
            Livestock            ,
            MechanicalParts      ,
            MicrofiberShielding  ,
            MiniatureElectronics ,
            Nanites              ,
            Oxides               ,
            Polyaramids          ,
            Polytextiles         ,
            RocketFuel           ,
            SilicateGlass        ,
            Superconductors      ,
            SupertensilePlastics ,
            SyntheticOil         ,
            TestCultures         ,
            Transmitter          ,
            ViralAgent           ,
            WaterCooledCPU       ,
        ]
    }

    fn t1s(self) -> (T1, T1) {
        use T1::*;
        use T2::*;
        match self {
            Biocells => (PreciousMetals, Biofuels),
            ConstructionBlocks => (ToxicMetals, ReactiveMetals),
            ConsumerElectronics => (ChiralStructures, ToxicMetals),
            Coolant => (Water, Electrolytes),
            EnrichedUranium => (ToxicMetals, PreciousMetals),
            Fertilizer => (Proteins, Bacteria),
            GenEnhancedLivestock => (Biomass, Proteins),
            Livestock => (Biofuels, Proteins),
            MechanicalParts => (PreciousMetals, ReactiveMetals),
            MicrofiberShielding => (Silicon, IndustrialFibers),
            MiniatureElectronics => (Silicon, ChiralStructures),
            Nanites => (ReactiveMetals, Bacteria),
            Oxides => (Oxygen, OxidizingCompound),
            Polyaramids => (IndustrialFibers, OxidizingCompound),
            Polytextiles => (IndustrialFibers, Biofuels),
            RocketFuel => (Electrolytes, Plasmoids),
            SilicateGlass => (Silicon, OxidizingCompound),
            Superconductors => (Water, Plasmoids),
            SupertensilePlastics => (Biomass, Oxygen),
            SyntheticOil => (Oxygen, Electrolytes),
            TestCultures => (Water, Bacteria),
            Transmitter => (ChiralStructures, Plasmoids),
            ViralAgent => (Biomass, Bacteria),
            WaterCooledCPU => (Water, ReactiveMetals),
        }
    }

    fn planets(self) -> Vec<Planet> {
        let (t10, t11) = self.t1s();
        Planet::all().iter().cloned().filter(|p| t10.planets().contains(p) && t11.planets().contains(p)).collect()
    }

    fn id(self) -> u64 {
        use T2::*;
        match self {
            Biocells             => 2329,
            ConstructionBlocks   => 3828,
            ConsumerElectronics  => 9836,
            Coolant              => 9832,
            EnrichedUranium      => 44,
            Fertilizer           => 3693,
            GenEnhancedLivestock => 15317,
            Livestock            => 3725,
            MechanicalParts      => 3689,
            MicrofiberShielding  => 2327,
            MiniatureElectronics => 9842,
            Nanites              => 2463,
            Oxides               => 2317,
            Polyaramids          => 2321,
            Polytextiles         => 3695,
            RocketFuel           => 9830,
            SilicateGlass        => 3697,
            Superconductors      => 9838,
            SupertensilePlastics => 2312,
            SyntheticOil         => 3691,
            TestCultures         => 2319,
            Transmitter          => 9840,
            ViralAgent           => 3775,
            WaterCooledCPU       => 2328,
        }
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }
}

impl fmt::Display for T2 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T2::*;
        write!(f, "{: >30}", match *self {
            Biocells             => "Biocells"             ,
            ConstructionBlocks   => "Construction Blocks"   ,
            ConsumerElectronics  => "Consumer Electronics"  ,
            Coolant              => "Coolant"              ,
            EnrichedUranium      => "Enriched Uranium"      ,
            Fertilizer           => "Fertilizer"           ,
            GenEnhancedLivestock => "Genetically Enhanced Livestock" ,
            Livestock            => "Livestock"            ,
            MechanicalParts      => "Mechanical Parts"      ,
            MicrofiberShielding  => "Microfiber Shielding"  ,
            MiniatureElectronics => "Miniature Electronics" ,
            Nanites              => "Nanites"              ,
            Oxides               => "Oxides"               ,
            Polyaramids          => "Polyaramids"          ,
            Polytextiles         => "Polytextiles"         ,
            RocketFuel           => "Rocket Fuel"           ,
            SilicateGlass        => "Silicate Glass"        ,
            Superconductors      => "Superconductors"      ,
            SupertensilePlastics => "Supertensile Plastics" ,
            SyntheticOil         => "Synthetic Oil"         ,
            TestCultures         => "Test Cultures"         ,
            Transmitter          => "Transmitter"          ,
            ViralAgent           => "Viral Agent"           ,
            WaterCooledCPU       => "Water-Cooled CPU"       ,
        })
    }
}

#[derive(Debug,Clone,Copy,PartialEq)]
enum T3 {
    BiotechResearchReports       ,
    CameraDrones                 ,
    Condensates                  ,
    CryoprotectantSolution       ,
    DataChips                    ,
    GelMatrixBiopaste            ,
    GuidanceSystems              ,
    HazmatDetectionSystems       ,
    HermeticMembranes            ,
    HighTechTransmitters         ,
    IndustrialExplosives         ,
    Neocoms                      ,
    NuclearReactors              ,
    PlanetaryVehicles            ,
    Robotics                     ,
    SmartfabUnits                ,
    Supercomputers               ,
    SyntheticSynapses            ,
    TranscranialMicrocontrollers ,
    UkomiSuperconductors         ,
    Vaccines                     ,
}

impl T3 {
    fn all () -> Vec<T3> {
        use T3::*;
        vec![
            BiotechResearchReports       ,
            CameraDrones                 ,
            Condensates                  ,
            CryoprotectantSolution       ,
            DataChips                    ,
            GelMatrixBiopaste            ,
            GuidanceSystems              ,
            HazmatDetectionSystems       ,
            HermeticMembranes            ,
            HighTechTransmitters         ,
            IndustrialExplosives         ,
            Neocoms                      ,
            NuclearReactors              ,
            PlanetaryVehicles            ,
            Robotics                     ,
            SmartfabUnits                ,
            Supercomputers               ,
            SyntheticSynapses            ,
            TranscranialMicrocontrollers ,
            UkomiSuperconductors         ,
            Vaccines                     ,
        ]
    }

    fn t2s (self) -> (T2,T2,Option<T2>) {
        use T3::*;
        use T2::*;
        match self {
            BiotechResearchReports       => (Nanites                , Livestock              , Some(ConstructionBlocks)    ),
            CameraDrones                 => (SilicateGlass          , RocketFuel             , None                     ),
            Condensates                  => (Oxides                 , Coolant                , None                     ),
            CryoprotectantSolution       => (TestCultures           , SyntheticOil           , Some(Fertilizer)            ),
            DataChips                    => (SupertensilePlastics   , MicrofiberShielding    , None                     ),
            GelMatrixBiopaste            => (Oxides                 , Biocells               , Some(Superconductors)       ),
            GuidanceSystems              => (WaterCooledCPU         , Transmitter            , None                     ),
            HazmatDetectionSystems       => (Polytextiles           , ViralAgent             , Some(Transmitter)           ),
            HermeticMembranes            => (Polyaramids            , GenEnhancedLivestock   , None                     ),
            HighTechTransmitters         => (Polyaramids            , Transmitter            , None                     ),
            IndustrialExplosives         => (Fertilizer             , Polytextiles           , None                     ),
            Neocoms                      => (Biocells               , SilicateGlass          , None                     ),
            NuclearReactors              => (MicrofiberShielding    , EnrichedUranium        , None                     ),
            PlanetaryVehicles            => (SupertensilePlastics   , MechanicalParts        , Some(MiniatureElectronics)  ),
            Robotics                     => (MechanicalParts        , ConsumerElectronics    , None                     ),
            SmartfabUnits                => (ConstructionBlocks     , MiniatureElectronics   , None                     ),
            Supercomputers               => (WaterCooledCPU         , Coolant                , Some(ConsumerElectronics)   ),
            SyntheticSynapses            => (SupertensilePlastics   , TestCultures           , None                     ),
            TranscranialMicrocontrollers => (Biocells               , Nanites                , None                     ),
            UkomiSuperconductors         => (SyntheticOil           , Superconductors        , None                     ),
            Vaccines                     => (Livestock              , ViralAgent             , None                     ),
        }
    }

    fn planets(self) -> Vec<Planet> {
        Planet::all().iter().cloned().filter(|p| {
            match self.t2s() {
                (t20,t21,Some(t22)) => {
                    let (t1a, t1b) = t20.t1s();
                    let (t1c, t1d) = t21.t1s();
                    let (t1e, t1f) = t22.t1s();
                    t1a.planets().contains(p) &&
                    t1b.planets().contains(p) &&
                    t1c.planets().contains(p) &&
                    t1d.planets().contains(p) &&
                    t1e.planets().contains(p) &&
                    t1f.planets().contains(p)
                } 
                (t20,t21,None) => {
                    let (t1a, t1b) = t20.t1s();
                    let (t1c, t1d) = t21.t1s();
                    t1a.planets().contains(p) &&
                    t1b.planets().contains(p) &&
                    t1c.planets().contains(p) &&
                    t1d.planets().contains(p)
                } 
            }
        }).collect()
    }

    fn id(self) -> u64 {
        use T3::*;
        match self {
            BiotechResearchReports       => 2358,
            CameraDrones                 => 2345,
            Condensates                  => 2344,
            CryoprotectantSolution       => 2367,
            DataChips                    => 17392,
            GelMatrixBiopaste            => 2348,
            GuidanceSystems              => 9834,
            HazmatDetectionSystems       => 2366,
            HermeticMembranes            => 2361,
            HighTechTransmitters         => 17898,
            IndustrialExplosives         => 2360,
            Neocoms                      => 2354,
            NuclearReactors              => 2352,
            PlanetaryVehicles            => 9846,
            Robotics                     => 9848,
            SmartfabUnits                => 2351,
            Supercomputers               => 2349,
            SyntheticSynapses            => 2346,
            TranscranialMicrocontrollers => 12836,
            UkomiSuperconductors         => 17136,
            Vaccines                     => 28974,
        }
    }

    fn url(self) -> String {
        format!("https://api.eve-central.com/api/marketstat/json?typeid={}&usesystem=30000142", self.id())
    }
}

impl fmt::Display for T3 {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        use T3::*;
        write!(f, "{: >30}", match *self {
            BiotechResearchReports        => "Biotech Research Reports" ,
            CameraDrones                  => "Camera Drones"  ,
            Condensates                   => "Condensates"  ,
            CryoprotectantSolution        => "Cryoprotectant Solution" ,
            DataChips                     => "Data Chips"  ,
            GelMatrixBiopaste             => "Gel-Matrix Biopaste" ,
            GuidanceSystems               => "Guidance Systems" ,
            HazmatDetectionSystems        => "Hazmat Detection Systems" ,
            HermeticMembranes             => "Hermetic Membranes"  ,
            HighTechTransmitters          => "High-Tech Transmitters"  ,
            IndustrialExplosives          => "Industrial Explosives"  ,
            Neocoms                       => "Neocoms" ,
            NuclearReactors               => "Nuclear Reactors" ,
            PlanetaryVehicles             => "Planetary Vehicles" ,
            Robotics                      => "Robotics" ,
            SmartfabUnits                 => "Smartfab Units"  ,
            Supercomputers                => "Supercomputers"  ,
            SyntheticSynapses             => "Synthetic Synapses" ,
            TranscranialMicrocontrollers  => "Transcranial Microcontrollers"  ,
            UkomiSuperconductors          => "Ukomi Superconductors"  ,
            Vaccines                      => "Vaccines"  ,
        })                                   
    }                                        
}                                            

#[derive(Serialize, Deserialize, Debug)]
struct ForQuery {
    bid: Option<bool>,
    types: Vec<u64>,
    regions: Vec<u64>,
    systems: Vec<u64>,
    hours: u64,
    minq: u64,
}

#[derive(Serialize, Deserialize, Debug)]
struct Unknown2 {
    forQuery: ForQuery,
    volume: u64,
    wavg: f64,
    avg: f64,
    variance: f64,
    stdDev: f64,
    median: f64,
    fivePercent: f64,
    max: f64,
    min: f64,
    highToLow: bool,
    generated: u64,
}

#[derive(Serialize, Deserialize, Debug)]
struct Unknown {
    buy: Unknown2,
    all: Unknown2,
    sell:Unknown2, 
}

trait Abbr {
    fn abbr (&self) -> String;
}

impl Abbr for Vec<Planet> {
    fn abbr (&self) -> String {
        Planet::all().iter().map(|p|if self.contains(p) { p.first_letter() } else { "_".to_owned() }).collect()
    }
}

fn main() {
    use Planet::*;
    //use hyper::Client;
    use std::error::Error;
    use curl::http;
    //use json_tools::Lexer;
    use itertools::Itertools;

    let j141332 = vec![
        Lava,
        Barren,
        Barren,
        Oceanic,
        Barren,
        Gas,
        Gas,
        Gas,
        Barren,
        Barren,
    ];

    println!("");
    println!("{}", j141332.abbr());
    println!("");

    // for planet in j141332 {
    //    println!("{:?}\t{:?}\t{:?}", planet, planet.t1s(), planet.t2s());
    // }

    //tier4: http://api.eve-central.com/api/marketstat?typeid=2867&typeid=2868&typeid=2869&typeid=2870&typeid=2871&typeid=2872&typeid=2875&typeid=2876&usesystem=30000142
    //tier3: http://api.eve-central.com/api/marketstat?typeid=2358&typeid=2345&typeid=2344&typeid=2367&typeid=17392&typeid=2348&typeid=9834&typeid=2366&typeid=2361&typeid=17898&typeid=2360&typeid=2354&typeid=2352&typeid=9846&typeid=9848&typeid=2351&typeid=2349&typeid=2346&typeid=12836&typeid=17136&typeid=28974&usesystem=30000142
    //tier2: http://api.eve-central.com/api/marketstat?typeid=2329&typeid=3828&typeid=9836&typeid=9832&typeid=44&typeid=3693&typeid=15317&typeid=3725&typeid=3689&typeid=2327&typeid=9842&typeid=2463&typeid=2317&typeid=2321&typeid=3695&typeid=9830&typeid=3697&typeid=9838&typeid=2312&typeid=3691&typeid=2319&typeid=9840&typeid=3775&typeid=2328&usesystem=30000142
    //tier1: http://api.eve-central.com/api/marketstat?typeid=3645&typeid=2397&typeid=2398&typeid=2396&typeid=2395&typeid=9828&typeid=2400&typeid=2390&typeid=2393&typeid=3683&typeid=2399&typeid=2401&typeid=3779&typeid=2392&typeid=2389&usesystem=30000142
    //tier0: http://api.eve-central.com/api/marketstat?typeid=2268&typeid=2305&typeid=2267&typeid=2288&typeid=2287&typeid=2307&typeid=2272&typeid=2309&typeid=2073&typeid=2310&typeid=2270&typeid=2306&typeid=2286&typeid=2311&typeid=2308&usesystem=30000142
        
//    for planet in Planet::all() {
//        println!("{:?}\t{:?}\t{:?}\t{:?}", planet, planet.t1s(), planet.t2s(), planet.t3s());
//    }

    //for t1 in T1::all() {
    //    println!("{}", t1.url());
    //}

    //TODO use svg to draw infographics (diagrams, graphs) for every planet

    /*
    //XXX use curl until #531 will be merged in (https://github.com/hyperium/hyper/issues/531)
    let client = Client::new();
    let res = match client.get(&T1::Bacteria.url()).send() {
        Ok(res) => res,
        Err(e) => {
            println!("{} {}", e.description(), match e.cause() {
                Some(s) => format!("({})", s),
                None => "".to_owned(),
            });
            return;
        }
    };
    assert_eq!(res.status, hyper::Ok);
    */

    let mut handle = http::handle();

    println!("");

    for t1 in T1::all() {
        let resp = handle.get(t1.url().as_str()).exec().unwrap();
        if resp.get_code() == 200 {
            let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
            let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
            println!("{} {:10.2} {:10.2} {}", t1, des[0].buy.avg, des[0].buy.wavg, t1.planets().abbr());
        }
    }

    println!("");

    for t2 in T2::all() {
        let resp = handle.get(t2.url().as_str()).exec().unwrap();
        if resp.get_code() == 200 {
            let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
            let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
            //let (t10, t11) = t2.t1s();
            println!("{} {:10.2} {:10.2} {}", t2, des[0].buy.avg, des[0].buy.wavg, t2.planets().abbr()/*, t10.planets(), t11.planets()*/);
        }
    }

    println!("");

    for t3 in T3::all() {
        let resp = handle.get(t3.url().as_str()).exec().unwrap();
        if resp.get_code() == 200 {
            let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
            let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
            println!("{} {:10.2} {:10.2} {}", t3, des[0].buy.avg, des[0].buy.wavg, t3.planets().abbr());
        }
    }

    /*
    println!("");

    for planet in j141332.iter().unique() {
        println!("{:?}", planet);
        for t1 in planet.t1s() {
            let resp = handle.get(t1.url().as_str()).exec().unwrap();
            if resp.get_code() == 200 {
                let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
                let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
                println!("{} {:10.2} {:10.2} {}", t1, des[0].buy.avg, des[0].buy.wavg, t1.planets().abbr());
            }
        }
        for t2 in planet.t2s() {
            let resp = handle.get(t2.url().as_str()).exec().unwrap();
            if resp.get_code() == 200 {
                let body = String::from_utf8(resp.get_body().to_owned()).unwrap();
                let des: Vec<Unknown> = serde_json::from_str(&body).unwrap();
                println!("{} {:10.2} {:10.2} {}", t2, des[0].buy.avg, des[0].buy.wavg, t2.planets().abbr());
            }
        }
        println!("");
    }
    */
}
